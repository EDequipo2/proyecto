/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package taskmanager;

/**
 *
 * @author juandros
 */
public class Nodo<T> {
    private T elemento;
    private Nodo<T> ant;
    private Nodo<T> sig;

    public Nodo(T elemento, Nodo<T> ant, Nodo<T> sig) {
        this.elemento = elemento;
        this.ant = ant;
        this.sig = sig;
    }

    public T getElemento() {
        return elemento;
    }

    public void setElemento(T elemento) {
        this.elemento = elemento;
    }

    public Nodo<T> getAnt() {
        return ant;
    }

    public void setAnt(Nodo<T> ant) {
        this.ant = ant;
    }

    public Nodo<T> getSig() {
        return sig;
    }

    public void setSig(Nodo<T> sig) {
        this.sig = sig;
    }

    @Override
    public String toString() {
        return " " + elemento + " ";
    }
    
    
}
