/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package taskmanager;

/**
 *
 * @author juandrosa
 */
public class ListaDoble<T> {
    private Nodo<T> inicio, fin;

    public ListaDoble() {
        inicio=null;
        fin=null;        
    }
    
    public boolean isEmpty(){
        return inicio==null || fin==null;
    }
    public void insertaInicio(T dato){
        Nodo<T> nodo = new Nodo(dato,null,inicio);
        if(inicio==null){
            fin=nodo;
        }else{
            if(fin.getAnt()==null){
                this.fin.setAnt(nodo);
            }else{
                inicio.setAnt(nodo);                
            }  
        }        
        this.inicio=nodo;        
    }
    
    public void insertaFin(T dato){
        Nodo<T> nodo = new Nodo(dato,fin,null);
        Nodo<T> aux = fin;
        this.fin=nodo;    
        aux.setSig(nodo);              
    }
    
    public String eliminaInicio(){
        if(inicio==null){
            return "Lista vacia, no se puede eliminar elemento";
        }
        if(inicio.getSig()==null){
            inicio=null;
            return "Primer elemento eliminado. La lista ha quedado vacía";
        }else{
            Nodo<T> aux = inicio;
            this.inicio=this.inicio.getSig();
            aux=null;
            return "Primer elemento eliminado";
        }
        
    }
    public String eliminaFin(){
        if(fin==null){
            return "Lista vacia, no se puede eliminar elemento";
        }
        if(fin.getAnt()==null){
            fin=null;           
            return "Ultimo elemento eliminado. La lista ha quedado vacia";
        }else{
            System.out.println(this.fin.getAnt().toString());
            fin=this.fin.getAnt();
            fin.setSig(null);
            return "Último elemento eliminado";
        }
    }
    
    public String eliminaEnPos(int pos){ 
        int lenght=this.getTamanio();
        if(lenght==0){
            return "No se puede eliminar en lista vacía";
        }else if(pos==0){
            this.eliminaInicio();
            return "Elemento eliminado correctamente";
        }else if(pos==lenght-1){
            this.eliminaFin();
            return "Elemento eliminado correctamente";
        }else{
            int i=0;
            Nodo<T> aux=inicio;
            while(i<pos){
                aux=aux.getSig();
                i++;
            }
            //el nodo aux es el que se quiere eliminar
            //redireccionamos
            aux.getAnt().setSig(aux.getSig());
            aux.getSig().setAnt(aux.getAnt());
            return "Elemento eliminado correctamente";
        }
        
    }
    public int getTamanio(){
        Nodo<T> aux=inicio;
        int i=0;
        while(aux!=null){
            aux=aux.getSig();
            i++;
        }
        return i;
    }
    
    public String toString(){
        //recorre la lista de Inicio a Fin
        if(isEmpty()){
            return "La lista está vacía";
        }else{
            Nodo<T> aux=this.inicio;
            String resultado="";
            while(aux!=null){
                resultado+= aux.toString();
                aux=aux.getSig();
            }
            return resultado;
        }
    }        
    
    public String toStringFin(){
        //recorre la lista de fin a inicio
        if(isEmpty()){
            return "La lista está vacía";
        }else{
            Nodo<T> aux=this.fin;
            String resultado="";
            while(aux!=null){
                resultado+= aux.toString();
                aux=aux.getAnt();
            }
            return resultado;
        }
    }
    
    public T getElementAt(int n){
        if(inicio==null){
            return null;
        }
        if(getTamanio()<n){
            return null;
        }
        int i=0;
        Nodo<T> aux=inicio;
        while(i<n){
            aux=aux.getSig();
            i++;
        }
        return aux.getElemento();
    }
    
    public int intercambio(int Pos1,int Pos2){
        Nodo<T> aux3;
        T auxDato;
        if(inicio==null){
            return 0;
        }
        if(getTamanio()<Pos1 || getTamanio()<Pos2){
            return 0;
        }
        int i=0;
        Nodo<T> aux1=inicio;
        while(i<Pos1){
            aux1=aux1.getSig();
            i++;            
        }        
        i=0;
        Nodo<T> aux2=inicio;
        while(i<Pos2){
            aux2=aux2.getSig();
            i++;            
        }
        auxDato=aux2.getElemento();
        aux2.setElemento(aux1.getElemento());
        aux1.setElemento(auxDato);                
        return 1;
    }
    
}
